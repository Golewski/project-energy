Binary and byte functions
=========================

**Energy.Base.Bit** class is a collection of binary utility functions.

Compare
-------

Compare byte arrays.

```csharp
int Energy.Base.Bit.Compare(byte[] left, byte[] right)
```

Reverse
-------

Reverse order of bytes.

Exchange lower byte with higher one

```csharp
ushort Energy.Base.Bit.Reverse(ushort value)
```

```csharp
uint Energy.Base.Bit.Reverse(uint value)
```

Endianess
---------

Take up to two 16-bit unsigned words and return them as 32-bit unsigned word.

MSB / Big-Endian.

```csharp
UInt32 Energy.Base.Bit.GetUInt32MSB(params UInt16[] array)
```

Take up to two 16-bit unsigned words and return them as 32-bit unsigned word.

LSB / Little-Endian.

```csharp
UInt32 Energy.Base.Bit.GetUInt32LSB(params UInt16[] array)
```

NOT
---

Perform bitwise NOT operation on every byte in array.

```csharp
byte[] Energy.Base.Bit.Not(byte[] array)
```

OR
--

Perform bitwise OR operation on every byte in array by second array.

Second array is treated as ring buffer when shorter than first one.

```csharp
byte[] Energy.Base.Bit.Or(byte[] one, byte[] two)
```

AND
---

Perform bitwise AND operation on every byte in array by second array.

Second array is treated as ring buffer when shorter than first one.

```csharp
byte[] Energy.Base.Bit.And(byte[] one, byte[] two)
```

XOR
---

Perform bitwise XOR operation on every byte in array by second array.

Second array is treated as ring buffer when shorter than first one.

```csharp
byte[] Energy.Base.Bit.Xor(byte[] one, byte[] two)
```

ROL
---

Rotate bits left in an array by given bit count.

When negative number of bits is given, right rotation will be performed instead.

```csharp
byte[] Energy.Base.Bit.Rol(byte[] array, int count)
```

ROR
---

Rotate bits right in an array by given bit count.

When negative number of bits is given, left rotation will be performed instead.

```csharp
byte[] Energy.Base.Bit.Ror(byte[] array, int count)
```
