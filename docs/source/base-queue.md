Queue class
===========

Queues are specifically designed to operate in a FIFO context (first-in first-out), where elements are inserted into one end of the list and extracted from the beginning.

**Energy.Base.Queue<T>** is a generic thread-safe class which can be used to implement FIFO queues in multithreading or asynchronous environment.

Additionally, the class provides access to events when adding and removing items from the queue.

The limit is also supported. If the *Circular* option is activated after the limit has been exceeded, the oldest items are removed from the list.

Example
-------

```csharp
using System;
					
public class Program
{
	public static void Main()
	{
		var q = new Energy.Base.Queue();
		var o1 = "123";
		var o2 = "abc";
		// insert object into queue by calling Push()
		q.Push(o1);
		q.Push(o2);
		q.Push(o1);
		q.Push(o2);
		q.Push(o2);
		q.Push(o2);
		object o;
		// retreive object from queue by calling Pull()
		while (null != (o = q.Pull()))
		{
			Console.WriteLine(o);
		}
	}
}
```

[Open in **dotnetfiddle.net**](https://dotnetfiddle.net/27IIYM)

Properties
----------

Numer of elements in queue.

```csharp
int Count
```

Check if queue is empty.

```csharp
bool IsEmpty
```

Limit number of items in queue.

```csharp
int Limit
```

Ring mode
---------

Makes internal buffer work like circular buffer.
When this option is set, the oldest items are removed from the list when limit has been exceeded.

```csharp
bool Ring
```

Example of circular buffer use.

```csharp
Energy.Base.Queue<string> queue = new Energy.Base.Queue<string>();
queue.Ring = true;
queue.Limit = 2;
queue.Push("A");
queue.Push("B");
queue.Push("C");
// number of element will be 2 because both limitand circular option are set
string value;
value = queue.Pull();
System.Diagnostics.Debug.WriteLine(value); // "B"
value = queue.Pull();
System.Diagnostics.Debug.WriteLine(value); // "C"
// queue is now empty, so next element will be null
value = queue.Pull();
queue.Ring = false;
bool success;
success = queue.Push("A"); // true
success = queue.Push("B"); // true
success = queue.Push("C"); // false because limit is reached
```

Functions
---------

Put element at the end of queue.
If limit is reached, function will return false and element will not be put
at the end of the queue unless Ring option is set.

```csharp
bool Energy.Base.Text.Push(T item)
```

Put array of elements at the end of queue.

```csharp
bool Energy.Base.Text.Push(T[] array)
```

Take first element from queue, remove it from queue, and finally return.
If queue is empty, function will return null.

```csharp
T Energy.Base.Text.Pull()
```

Take number of elements from queue, remove them and return array of elements taken.
**Pull(0)** will return all elements from queue and empty it.

```csharp
T[] Energy.Base.Text.Pull(int count)
```

Take element from queue with specified time limit to wait
for new item to come.
It will pause invoking thread as it is expected to do so.

```csharp
T Energy.Base.Text.Pull(double timeout)
```

Put element back to queue, at begining. 
This element will be taken first.

```csharp
void Energy.Base.Text.Back(T item)
```

Put array of elements back to queue, at begining.
These elements will be taken first.

```csharp
void Energy.Base.Text.Back(T[] list)
```

Delete last element from queue and return it.

```csharp
T Energy.Base.Text.Chop()
```

Delete number of last elements from queue and return them.

```csharp
T[] Energy.Base.Text.Chop(int count)
```

Events
------

Event fired when Push() is called and element was added to the queue.

```csharp
event Energy.Base.Anonymous.Event OnPush
```

Event fired when Pull() is called and element was taken from the queue.

```csharp
event Energy.Base.Anonymous.Event OnPull
```

Event fired when Back() is called and element was put back to the queue.

```csharp
event Energy.Base.Anonymous.Event OnBack
```
Event fired when Chop() is called and element was deleted from the queue.

```csharp
event Energy.Base.Anonymous.Event OnChop
```
