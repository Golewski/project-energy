Clock, date and time
====================

**Energy.Base.Clock** contains functions related to classes, objects and assemblies.

You will find here functions to modify fields or properties of C# objects.

CurrentTime
-----------

Return current date in ISO format.

Example: "2020-01-01".

```csharp
string Energy.Base.Clock.CurrentDate
```

CurrentTime
-----------

Return current time as time string with millisecond part in 24h format.

Example: "17:33:15.176".

```csharp
string Energy.Base.Clock.CurrentDate
```

CurrentTimeSpace
----------------

Return current time as time string with millisecond part in 24h format and trailing space.
Example: "17:33:15.176 ".

```csharp
string Energy.Base.Clock.CurrentTimeSpace
```

Floor
-----

Round time to specified precision.

```csharp
DateTime Energy.Base.Clock.Floor(DateTime value, int precision)
```

Precision may be positive or negative number.
On positive precision function will include as many fractional second digits as possible (up to 7).
On negative precision function may round to minute, hour or even a year.
Use 3 for milliseconds, 6 for microseconds, -2 for minutes, -4 for hours, etc.

```csharp
DateTime needle;
DateTime result;
needle = new DateTime(2121, 12, 16, 21, 17, 33, 456);
result = Energy.Base.Clock.Floor(needle, 4);   // 2121-12-16 21:17:33.456
result = Energy.Base.Clock.Floor(needle, 3);   // 2121-12-16 21:17:33.456
result = Energy.Base.Clock.Floor(needle, 2);   // 2121-12-16 21:17:33.450
result = Energy.Base.Clock.Floor(needle, 1);   // 2121-12-16 21:17:33.400
result = Energy.Base.Clock.Floor(needle, 0);   // 2121-12-16 21:17:33
result = Energy.Base.Clock.Floor(needle, -1);  // 2121-12-16 21:17:30
result = Energy.Base.Clock.Floor(needle, -2);  // 2121-12-16 21:17:00
result = Energy.Base.Clock.Floor(needle, -3);  // 2121-12-16 21:10:00
result = Energy.Base.Clock.Floor(needle, -4);  // 2121-12-16 21:00:00
result = Energy.Base.Clock.Floor(needle, -5);  // 2121-12-16 20:00:00
result = Energy.Base.Clock.Floor(needle, -6);  // 2121-12-16 00:00:00
result = Energy.Base.Clock.Floor(needle, -7);  // 2121-12-10 00:00:00
result = Energy.Base.Clock.Floor(needle, -8);  // 2121-12-01 00:00:00
result = Energy.Base.Clock.Floor(needle, -9);  // 2121-10-01 00:00:00
result = Energy.Base.Clock.Floor(needle, -10); // 2121-01-01 00:00:00
result = Energy.Base.Clock.Floor(needle, -11); // 2120-01-01 00:00:00
result = Energy.Base.Clock.Floor(needle, -12); // 2100-01-01 00:00:00
result = Energy.Base.Clock.Floor(needle, -13); // 2000-01-01 00:00:00
result = Energy.Base.Clock.Floor(needle, -14); // 0001-01-01 00:00:00
```

Truncate
--------

Round down DateTime value to desired precision of seconds.

```csharp
DateTime Energy.Base.Clock.Truncate(DateTime value, int precision)
```

Round down DateTime to whole seconds

```csharp
DateTime Energy.Base.Clock.Truncate(DateTime value)
```

Round down TimeSpan to desired precision of seconds.

```csharp
TimeSpan Energy.Base.Clock.Truncate(TimeSpan value, int precision)
```

HasFractionalPart
-----------------

Check if DateTime value contains fractional part of seconds.

```csharp
bool Energy.Base.Clock.HasFractionalPart(DateTime value)
```

Check if TimeSpan value contains fractional part of seconds.

```csharp
bool Energy.Base.Clock.HasFractionalPart(TimeSpan value)
```

IsLeapYear
----------

Sample subroutine to calculate if a year is a leap year.

```csharp
bool Energy.Base.Clock.IsLeapYear(int year)
```
