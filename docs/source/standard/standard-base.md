Energy Base Standard
====================

Text
----

### Trim ###

Remove all leading and trailing whitespace characters from text.

Following characters are treated as whitespace: space character ``" "`` (code 32), horizontal tab ``"\t"`` (code 09), line feed ``"\n"`` (code 10), carriage return ``"\r"`` (code 13), vertical tab ``"\v"`` (code 11), form feed ``"\f"`` (code 12), null character ``"\0"`` (code 0).

### IsInteger ###

Check if value represents integer number.

### IsNumber ###

Check if value represents number.

12.e3

12313,123E+123

Cast
----

### StringToBool ###

Convert text to boolean value.

### StringToInt ###

Convert text to signed 32-bit integer number without exception ignoring leading and trailing whitespace characters.

If conversion cannot be performed, default value 0 is returned.

### StringToLong ###

Convert text to signed 64-bit long integer number without exception.

If conversion can't be done, 0 will be returned.

