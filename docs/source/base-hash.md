Hashing functions
=================

Several hashing functions for sequences of characters or bytes are available.

MD5
---

Return MD5 hash for a string.

```csharp
string Energy.Base.Hash.MD5(string text, Encoding encoding)
```

```csharp
string Energy.Base.Hash.MD5(string text)
```

SHA-1
-----

Return SHA-1 hash for a string.

```csharp
string Energy.Base.Hash.SHA1(string text, Encoding encoding)
```

```csharp
string Energy.Base.Hash.SHA1(string text)
```

SHA-256
-------

Return SHA-256 (SHA-2) hash for a string.

```csharp
string Energy.Base.Hash.SHA256(string text, Encoding encoding)
```

```csharp
string Energy.Base.Hash.SHA256(string text)
```

SHA-384
-------

Return SHA-384 (SHA-2) hash for a string.

```csharp
string Energy.Base.Hash.SHA384(string text, Encoding encoding)
```

```csharp
string Energy.Base.Hash.SHA384(string text)
```

SHA-512
-------

Return SHA-512 (SHA-2) hash for a string.

```csharp
string Energy.Base.Hash.SHA512(string text, Encoding encoding)
```

```csharp
string Energy.Base.Hash.SHA512(string text)
```

CRC
---

For each characters do a 5-bit left circular shift and XOR in character numeric value (CRC variant).

```csharp
string Energy.Base.Hash.CRC(string value)
```

```csharp
string Energy.Base.Hash.CRC2(string value)
```

CRC-16-CCITT
------------

Calculate 16-bit CRC-CCITT checksum with specified polynominal and initial value.

```csharp
ushort Energy.Base.Hash.CRC16CCITT(byte[] array, ushort poly, ushort init)
```

```csharp
ushort Energy.Base.Hash.CRC16CCITT(string text, ushort poly, ushort init)
```

PJW
---

For each characters add character numeric value and left shift by 4 bits (PJW hash).

```csharp
uint Energy.Base.Hash.PJW(string value)
```
