Hexadecimal
===========

Functions that support hexadecimal numeral system conversions.

ArrayToHex
----------

Convert byte array to hexadecimal string.

```csharp
string Energy.Base.Hex.ArrayToHex(byte[] array)
```

```csharp
string Energy.Base.Hex.ArrayToHex(byte[] array, string space)
```

HexToArray
----------

Convert hexadecimal string to byte array.

This is strict version of conversion function.

Hexadecimal string should contain only digits and small or upper letters A-F.
Any other character is treated as zero.

```csharp
byte[] Energy.Base.Hex.HexToArray(string hex)
```

This version of conversion function allows to use whitespace characters.

```csharp
byte[] Energy.Base.Hex.HexToArray(string hex, bool ignoreWhite)
```

This version of conversion function allows to use
prefixes (like "0x" or "$") and whitespace characters.

```csharp
byte[] Energy.Base.Hex.HexToArray(string hex, bool ignoreWhite, string[] prefix)
```

```csharp
var b = Energy.Base.Hex.HexToArray("0x 12 33", true, new string[] { "0x" });
```

BinToHex
--------

Convert binary string to hexadecimal string.

```csharp
string Energy.Base.Hex.BinToHex(string bin)
```

HexToBin
--------

Convert hexadecimal string to binary string.

Note that hexadecimal "0" will be represented with leading zeroes as "0000" in binary.
Resulting binary string will always have a length divisible by 4.

Works also when hexadecimal string starts with "0x" or "$".

```csharp
string Energy.Base.Hex.HexToBin(string hex)
```

HexToByte
---------

Convert hexadecimal string to byte value (System.Byte).

```csharp
byte Energy.Base.Hex.HexToByte(string hex)
```

RemovePrefix
------------

Remove leading prefix "0x", "0X" or "$" from hexadecimal string.

```csharp
string Energy.Base.Hex.RemovePrefix(string hex)
```

ByteToPrintable
---------------

Convert byte to printable character.

All non-ASCII characters will be represented as dot character.
Bytes 0, 128 and 255 will be represented as space.

```csharp
char Energy.Base.Hex.ByteToPrintable(byte b)
```
