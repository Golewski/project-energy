Byte array builder
==================

Use ByteArrayBuilder to build byte arrays with MemoryStream.

This class is not thread safe for performance reasons.

You must use your own locking mechanism to achieve thread safety.

Objects of **Energy.Base.ByteArrayBuilder** works like **streams** supporting seeking from the beginning of the stream.
You may also call *Rewind()* or *Tail()*.

```csharp
Energy.Base.ByteArrayBuilder b = new Energy.Base.ByteArrayBuilder();
b.WriteByte(1);
b.WriteByte(2);
b.Rewind();
t = b.ReadByte();
b.Tail();
b.WriteByte(3);
b.Seek(1);
t = b.ReadByte();
```

Automatic size of data for primitive types.

```csharp
var value = new int[] { 1, 2, 3, 4 };
var result = Energy.Base.ByteArrayBuilder.ToByteArray(value);
var expect = new byte[] { 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 3, 0, 0, 0, 4 };
```

**Energy.Base.ByteArrayBuilder** also supports Base64 format.

```csharp
Energy.Base.ByteArrayBuilder bb;
bb = new Energy.Base.ByteArrayBuilder();
string s1 = "TWljcm9zb2Z0IFdpbmRvd3M=";
bb.WriteBase64(s1);
bb.Rewind();
string s2 = bb.ReadString();
// s2 equals "Microsoft Windows"
bb.Clear();
string s3 = "Gęś";
bb.WriteString(s3);
bb.Rewind();
string s4 = bb.ReadBase64();
// s4 equals "R8SZxZs="
```
