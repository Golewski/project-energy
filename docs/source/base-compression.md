Compression
===========

Deflate
-------

Compress using deflate algorithm.

```csharp
byte[] Energy.Base.Compression.Compress(byte[] data)
```

Decompress using deflate algorithm.

```csharp
byte[] Energy.Base.Compression.Decompress(byte[] data)
```

GZip
----

Compress using gzip algorithm.

```csharp
byte[] Energy.Base.Compression.Compress(byte[] data)
```

Decompress using gzip algorithm.

```csharp
byte[] Energy.Base.Compression.Decompress(byte[] data)
```

Not much... yet... :-)
