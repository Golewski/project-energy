Text editor
===========

Editor
------

Text editor class.

```csharp
Energy.Core.Text.Editor editor;
// create editor object
editor = new Energy.Core.Text.Editor();
// or take global default
editor = Energy.Core.Text.Editor.Default;
```

### InsertBeforeFirstLine ###

Insert text before first line.

```csharp
string InsertBeforeFirstLine(string text, string line)
```

### AppendAfterFirstLine ###

Append text after first line.

```csharp
string AppendAfterFirstLine(string text, string line)
```

### InsertBeforeSecondLine ###

Insert text before second line.

```csharp
string InsertBeforeSecondLine(string text, string line)
```

### InsertBeforeLastLine ###

Insert text before last line.

```csharp
string InsertBeforeLastLine(string text, string line)
```

### AppendAfterLastLine ###

Append text after last line.

```csharp
string AppendAfterLastLine(string text, string line)
```

### GetLastLine ###

Get last line of text.

```csharp
string GetLastLine(string text)
```

### EnsureNewLineAtEnd ###

Ensure text ends with newline. Add newline string to the end if not included even if empty.

Works with multiple newline strings from **Energy.Base.Text.NEWLINE_ARRAY**.

```csharp
string EnsureNewLineAtEnd(string text)
```

### AppendAfterLastLine ###

Append text after last line.

```csharp
string AppendAfterLastLine(string text, string line)
```

### ConvertNewLine ###

Convert new line delimiter to specified one.

```csharp
string ConvertNewLine(string text, string newLine)
```

Convert newline delimiter to environment default.
Value of constant **Energy.Base.Text.NL** is used.

```csharp
string ConvertNewLine(string text)
```
