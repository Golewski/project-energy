Classes, objects and assemblies
=============================

**Energy.Base.Class** contains functions related to classes, objects and assemblies.

You will find here functions to modify fields or properties of C# objects.


GetDefault
----------

Get default value of specified type.

```csharp
object Energy.Base.Class.GetDefault(System.Type type)
```

```csharp
T Energy.Base.Class.GetDefault<T>()
```

GetFieldsAndProperties
----------------------

Get list of names of all fields and properties of specified class type.

```csharp
string[] Energy.Base.Class.GetFieldsAndProperties(Type type, bool includePrivate, bool includePublic)
```

```csharp
string[] Energy.Base.Class.GetFieldsAndProperties(Type type, bool includePrivate)
```

Get list of names of public fields and properties of specified class type.

```csharp
string[] Energy.Base.Class.GetFieldsAndProperties(Type type)
```

GetFieldOrPropertyAttribute
---------------------------

Get first attribute value for a field or property of desired class.

```csharp
object Energy.Base.Class.GetFieldOrPropertyAttribute(Type type, string name, Type attribute)
      
```

```csharp
T Energy.Base.Class.GetFieldOrPropertyAttribute<T>(Type type, string name)      
```

GetFieldOrPropertyAttributes
----------------------------

Get custom attributes of field or property of a class.

```csharp
object[] Energy.Base.Class.GetFieldOrPropertyAttributes(Type type, string field, Type filter, bool inherit, bool ignoreCase)
```

```csharp
T[] Energy.Base.Class.GetFieldOrPropertyAttributes<T>(Type type, string field, bool inherit, bool ignoreCase)
```

```csharp
object[] Energy.Base.Class.GetFieldOrPropertyAttributes(Type type, string field, Type filter)
```

GetTypesWithInterface
---------------------

Filter types that implements specified interface.

```csharp
System.Type[] Energy.Base.Class.GetTypesWithInterface(System.Type[] types, System.Type interfaceType)
```

GetTypeWithInterface
--------------------

Get first type that implements specified interface.
Return null if not found.

```csharp
System.Type Energy.Base.Class.GetTypeWithInterface(System.Type[] types, System.Type interfaceType)
```

GetTypesWithAttribute
---------------------

Filter types that have specified attribute.

```csharp
System.Type[] Energy.Base.Class.GetTypesWithAttribute(System.Type[] types, System.Type attributeType)
```

GetTypeWithAttribute
--------------------

Get first type having specified attribute.
Return null if not found.

```csharp
System.Type Energy.Base.Class.GetTypeWithAttribute(System.Type[] types, System.Type attributeType)
```

GetClassAttribute
-----------------

Get desired attribute for a class or null if not found.

```csharp
object Energy.Base.Class.GetClassAttribute(Type type, Type attribute)
```

```csharp
T Energy.Base.Class.GetClassAttribute<T>(Type type)
```

IsStatic
--------

True if class is static and cannot be instantiated.

```csharp
bool Energy.Base.Class.IsStatic(Type type)
```

IsInstance
----------

True if object of specified class can be created.
At least one public constructor needs to be found.
Function will result false for for static class type.

```csharp
bool Energy.Base.Class.IsInstance(Type type)
```

HasParameterlessConstructor
---------------------------

True if class has public parameterless constructor.

```csharp
bool Energy.Base.Class.HasParameterlessConstructor(Type type)
```

HasParameteredConstructor
---------------------------

True if class has at least one public constructor with specified number parameters.

```csharp
bool Energy.Base.Class.HasParameteredConstructor(Type type, int minimumParameterCount, int maximumParameterCount)
```

True if class has constructor with one or more parameters.

```csharp
bool Energy.Base.Class.HasParameteredConstructor(Type type)
```

Mangle
------

Mangle object by applying a function to each field and property of specified type.
Returns number of fields and properties affected.

```csharp
int Energy.Base.Class.Mangle<T>(object subject, bool includePrivate, bool includePublic
    , Energy.Base.Anonymous.Function<T, T> function)
```

```csharp
var o = new { Text = " Text " };
Energy.Base.Class.Mangle<string>(o, delegate (string _) { return (_ as string ?? "").Trim(); });
```

Mangle object by applying a function to each public field and property of specified type.
Returns number of fields and properties affected.

```csharp
int Energy.Base.Class.Mangle<T>(object subject, Energy.Base.Anonymous.Function<T, T> function)
```

Mangle object by applying a function to public field or property of specified class type.
Returns 1 if value was changed, 0 if not found or read only.

```csharp
int Energy.Base.Class.Mangle<T>(object subject, string name, bool includePrivate, bool includePublic
    , Energy.Base.Anonymous.Function<T, T> function)
```

```csharp
var msg = new TextClass() { Text = "" };
if (1 == Energy.Base.Class.Mangle<string>(msg, "Text", delegate { return "Hello"; }))
{
    Debug.WriteLine("I just set Text field or property to " + msg.Text);
}
var anon = new { Text = (string)null };
if (0 == Energy.Base.Class.Mangle<string>(anon, "Text", delegate { return "Hello"; }))
{
    Debug.WriteLine("I can't change anything in anonymous object");
}
```

Mangle object by applying a function to public field or property of specified class type.
Returns 1 if value was changed, 0 if not found or read only.

```csharp
int Energy.Base.Class.Mangle<T>(object subject, string name, Energy.Base.Anonymous.Function<T, T> function)
```

GetAssemblyFile
-------------------

Get filename of assembly.

This is just a simple alias for Location field of assembly object.

```csharp
string Energy.Base.Class.GetAssemblyFile(System.Reflection.Assembly assembly)
```

GetAssemblyDirectory
------------------------

Get directory name of assembly.

```csharp
string Energy.Base.Class.GetAssemblyDirectory(System.Reflection.Assembly assembly)
```
