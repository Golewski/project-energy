<style>
div.width-100 { width: 100% }
div.table-width-100 table { width: 100% }
div.align-left td, div.align-left th { text-align: left }
div.align-right-2 td:nth-child(2) { text-align: right }
div.align-right-3 td:nth-child(3) { text-align: right }
div.align-center td { text-align: center }
div.align-center-2 td:nth-child(2) { text-align: center }
div.align-center-3 td:nth-child(3) { text-align: center }
</style>

Database access
===============

**Energy.Core.Source** provides access to any SQL database source.
It uses **ADO.NET** so any compatible driver can be used.

How this can be helpful?

It gives bunch of primitive functions that can be done using SQL database.
That means executing queries, reading data from them, and maintain a connection if needed.
This class also provides error handling.

Higher levels of database abstractions like models or ORM mapping are not concerned here. 

Connection
----------

Create **Energy.Source.Connection** object to enable database access layer operations.

You need to specify vendor class which implements **IDbConnection** interface from **ADO.NET**.

```csharp
Energy.Source.Connection(Type vendor)
```
```csharp
Energy.Source.Connection(Type vendor, string connectionString)
```
```csharp
Energy.Source.Connection<Type>()
```
```csharp
Energy.Source.Connection<Type>(string connectionString)
```

### Vendor ###

<!--
| Provider  | Class                                  |
|-----------|----------------------------------------|
| MySQL     | MySql.Data.MySqlClient.MySqlConnection |
| SqlServer | System.Data.SqlClient.SqlConnection    |
| SQLite    | System.Data.SQLite.SQLiteConnection    |
-->

<div class="width-100 table-width-100 align-left">

<table>
<thead>
<tr>
<th>Provider</th>
<th>Class</th>
</tr>
</thead>
<tbody>
<tr>
<td>MySQL</td>
<td>MySql.Data.MySqlClient.MySqlConnection
</tr>
<tr>
<td>SqlServer</td>
<td>System.Data.SqlClient.SqlConnection</td>
</tr>
<tr>
<td>SQLite</td>
<td>System.Data.SQLite.SQLiteConnection</td>
</tr>
</tbody>
</table>

</div>

### Example ###

```csharp
Energy.Source.Connection<MySql.Data.MySqlClient.MySqlConnection> db;
db = new Energy.Source.Connection<MySql.Data.MySqlClient.MySqlConnection>();
db.ConnectionString = @"Server=127.0.0.1;Database=test;Uid=test;Pwd=test;";
```

Execute
-------

Execute SQL query.

For UPDATE, INSERT, and DELETE statements, the return value is the number of rows affected by the command. 

For all other types of statements, the return value is -1.

On error, return value is -2.

```csharp
int Execute(string query, out string error)
```

```csharp
int Execute(string query)
```

Load
----

Load data from query into DataTable.

```csharp
DataTable Load(string query, out string error)
```

```csharp
DataTable Load(string query)
```

Read
----

Read query results into DataTable.

This function will populate values in a loop using IDataReader. 

```csharp
DataTable Read(string query, out string error)
```

```csharp
DataTable Read(string query)
```

Fetch
-----

Fetch query results into Energy.Base.Table.

```csharp
Energy.Base.Table Fetch(string query, out string error)
```

```csharp
Energy.Base.Table Fetch(string query)
```

Scalar
------

Execute SQL query and read scalar value as a result.

```csharp
object Scalar(string query, out string error)
```

```csharp
object Scalar(string query)
```

```csharp
T Scalar<T>(string query, out string error)
```

```csharp
T Scalar<T>(string query)
```

Clone
-----

Return copy of object.

```csharp
object Clone()
```

```csharp
Energy.Source.Connection Copy()
```

Persistent
----------

If you want to limit connections to your database source you may turn on pertistent option by setting **Persistent** to **true**.

It's not normally required to do that so at database access layer, but you still might need it in some particular cases.

Only one **ADO.NET** connection object will be used through multiple executions. 
This way concurrent SQL executions will have to wait which is acquired with standard .NET locking mechanism. 
That will have performance impact for sure, although it might not be an issue.

Useful when using in-memory database connections created per connection.

Running hundreds or thousands of operations on the database may cause performance results to be different when using persistent option.

Events
------

Event fired when vendor connection object is created by Activator.

```csharp
event EventHandler OnCreate;
```

Event fired when vendor connection is open.

```csharp
event EventHandler OnOpen;
```

Event fired when vendor connection was closed.

```csharp
event EventHandler OnClose;
```

GetErrorText
------------

Get error text.

```csharp
string GetErrorText()
```

