XML support
===========

Serialize
---------

Serialize object to XML.

Object class must implement **IXmlSerializable** interface.

```csharp
string Energy.Base.Xml.Serialize(object data, string root, string space)
```

```csharp
string Energy.Base.Xml.Serialize(object data, string root)
```

```csharp
string Energy.Base.Xml.Serialize(object data)
```

### Example ###

```csharp
string xml = Energy.Base.Xml.Serialize(myObject, "Root", "org.example.xns");
```

Deserialize
-----------

Deserialize object from XML, root alternatives allowed.

```csharp
object Energy.Base.Xml.Deserialize(string content, Type type, string[] root, string space)
```

```csharp
object Energy.Base.Xml.Deserialize(string content, Type type, string root, string space)
```

```csharp
object Energy.Base.Xml.Deserialize(string content, Type type, string root)
```

```csharp
object Energy.Base.Xml.Deserialize(string content, Type type)
```

 Generic XML deserialization methods are also available.

```csharp
TDeserialize Energy.Base.Xml.Deserialize<TDeserialize>(string content, Type type, string[] root, string space)
```

```csharp
TDeserialize Energy.Base.Xml.Deserialize<TDeserialize>(string content)
```

ExtractRoot
-----------

Extract root element from XML.

```csharp
string Energy.Base.Xml.ExtractRoot(string xml)
```

ExtractRootShort
----------------

Extract root element without namespace from XML.

```csharp
string Energy.Base.Xml.ExtractRootShort(string xml)
```

Encode
------

Encode special characters with valid XML entities.

Only ASCII control codes and XML special characters will be encoded. All other valid UTF-8 characters will remain untouched.

Control characters like new line, carriage return, and tab will not be encoded either.

```csharp
string Energy.Base.Xml.Encode(string text)
```

Encode special characters with valid XML entities.

When encoding parameter is set to Encoding.UTF-8 then only ASCII control codes and XML special characters will be encoded. All other valid UTF-8 characters will remain untouched.

Control characters like new line, carriage return, and tab will not be encoded either.

When encoding parameter is set to Encoding.ASCII then additionaly all characters with codes higher that 126 will be encoded as character entities.

When encoding parameter is set to Encoding.Unicode then Unicode surrogate pairs (i.e. emoji) will also be encoded as character entities.

```csharp
string Energy.Base.Xml.Encode(string text, Encoding encoding)
```

Decode
------

Decode named or numeric character XML entities with corresponding characters.

```csharp
string Energy.Base.Xml.Decode(string text)
```
