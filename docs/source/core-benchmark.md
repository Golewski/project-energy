Benchmarking
============

Loop
----

Repeat action for a specified time and return number of iterations done during the specified time. If time was not specified function will result -1. If no operation has been performed before the specified time has elapsed, the function will return a zero value.

### Prototype ###

```csharp
static int Energy.Core.Benchmark.Loop(Energy.Base.Anonymous.Function, TimeSpan)
```

### Example ###

```csharp
int count = Energy.Core.Benchmark.Loop(() =>
{
    int next = r.Next();
    string text = next.ToString();
    int.TryParse(text, out next);
}, TimeSpan.FromSeconds(1.0));
Energy.Core.Tilde.WriteLine("Done ~m~default ~w~TryParse~0~ " + count);
```

Profile
-------

Perform a profiling operation by launching the action the specified number of times and returning the result of the profiling.

### Prototype ###

```csharp
static Energy.Core.Benchmark.Result Profile(Energy.Base.Anonymous.Function, int, int)
static Energy.Core.Benchmark.Result Profile(Energy.Base.Anonymous.Function, int, string)
static Energy.Core.Benchmark.Result Profile(Energy.Base.Anonymous.Function, int, int, string)
static Energy.Core.Benchmark.Result Profile(Energy.Base.Anonymous.Function, int)
static Energy.Core.Benchmark.Result Profile(Energy.Base.Anonymous.Function)
```

### Example ###

```csharp
Energy.Core.Benchmark.Result benchmark;
benchmark = Energy.Core.Benchmark.Profile(() =>
{
    long m = 0;
    for (int i = 0, n = 0; i < 100000; i++)
    {
        string s = i.ToString();
        if (Energy.Base.Text.TryParse(s, out n))
        {
            m += n;
        }
    }
}, 3, 1, "SumTryParse");
Energy.Core.Tilde.WriteLine(benchmark.ToString());
```

### Output ###

```
Garbage collected in 0.002 s
Time taken by SumTryParse in 3 iterations 0.047 s
Average time of execution 0.024 s
```
