Command line arguments
======================

Brand new class **Energy.Base.Command.Arguments** appeared here to ease implement command line options for everybody.

This class is build with [Named Parameter Idiom](http://www.cs.technion.ac.il/users/yechiel/c++-faq/named-parameter-idiom.html) design pattern.

Argument parsing mechanism was inspired by excelent npm package **yargs** for JavaScript.

```csharp
var argv = new Energy.Base.Command.Arguments(args)
    .Switch("help")
    .Switch("quiet")
    .Parameter("input")
    .Parameter("output")
    .Alias("?", "help")
    .Alias("q", "quiet")
    .Alias("i", "input")
    .Alias("o", "input")
    .Help("input", "Input file")
    .Help("output", "Output file")
    .Parse();

if (!argv["help"].Empty)
{
    Console.WriteLine("Help");
}
```

## Constructor ##

```csharp
Energy.Base.Command.Arguments(string[] args)
```

```csharp
Energy.Base.Command.Arguments(string line)
```

You can still add argument line later.

```csharp
Energy.Base.Command.Arguments()
```

## Parameter ##

Add command line parameter that will consume one
or more trailing arguments and provide them as values.

If you set count to 0, it will become switch (flag).

```csharp
Arguments Parameter(string name, int count)
```

Add command line parameter that will consume one
next trailing argument and provide it as value.

```csharp
Arguments Parameter(string name)
```

Special one.

Treat all unknown options as parametered.

```csharp
Arguments Parameter()
```

## Switch ##

Add single command line switch option, known also as flag.
Like "-v", or "--version".

```csharp
Arguments Switch(string name)
```

Special one.

Treat all unknown options as simple switches.

```csharp
Arguments Switch()
```

## Line ##

Add arguments from text line.

Arguments are divided by any whitespace character.


Arguments may use double quote (") character to include whitespace,
and multiple quoting is allowed within one argument.

For example: C:\"Documents and settings"\"Program Files"\
will be considered as one argument.

```csharp
Arguments Line(string line)
```

## Skip ##

Skip first n entries when parsing arguments.

```csharp
Skip(int skip)
```

## Strict ##
            
Set strict mode.

When strict mode is set, exception will be thrown on unrecognized option name.

```csharp
Arguments Strict(bool strict)
```

## Slash ##

Allow usage of slash options (starting with "/").

Allows to use DOS style options like "/?".

It's RSX-11 (and other similar DEC systems), through CP/M to MS-DOS legacy.

Should be avoided probably.

```csharp
Arguments Slash(bool slash)
```

## Short ##
            
Allow usage of short options (starting with "-").

Turned on as default.

```csharp
Arguments Short(bool enable)
```

## Long ##
            
Allow usage of long options (starting with "--").

Turned on as default.

```csharp
Arguments Long(bool enable)
```

## Alias ##
            
Add alias to parameter key.

```csharp
Arguments Alias(string alias, string name)
```

## Help ##
                
Add help description for parameter key.

```csharp
Arguments Help(string name, string description)
```

## Title ##
                
Add optional program title to be printed.

```csharp
Arguments Title(string title)
```

## About ##
                
Add additional description to be printed after optional title.

```csharp
Arguments About(string about)
```

## Usage ##
                
Set usage information printed after optional title and optional about information.

```csharp
Arguments Usage(string title)
```

## Greetings ##
                
Set greetings information to be printed at the end.

```csharp
Arguments Greetings(string title)
```

## Print ##

Return text to be printed for command line usage description.

```csharp
var argv = new Energy.Base.Command.Arguments(args)
    .Title("Program title")
    .About("Short or longer program description")
    .Switch("help")
    .Switch("version")
    .Alias("?", "help")
    .Alias("V", "version")
    .Help("help", "Show this information")
    .Help("version", "Display version number")
    .Parse();

if (argv["help"].IsTrue)
{
    Console.Write(argv.Print());
    return;
}
```

If ``--help`` or ``-?`` switch is used, program will print text description.

```
Program title

Short or longer program description

    --help
    -?

    Show this information


    --version
    -V

    Display version number

```

## Parse ##

Probably most important function here.

Parse arguments and set values for options.

```csharp
Arguments Parse()
```

Parameterless method will parse arguments set by constructor or modified later.

When calling with parameters, arguments to be parsed will only be taken from invoker.

```csharp
Arguments Parse(string line)
```

```csharp
Arguments Parse(string[] args)
```
