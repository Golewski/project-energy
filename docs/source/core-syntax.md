Variable syntax parser
======================

Example
-------

```csharp
string interpolatedText;
interpolatedText = Energy.Core.Syntax.Default.Parse("{{HOUR_12}}:{{MM}}:{{SS}}.{{MS}} {{AM}}");
```
