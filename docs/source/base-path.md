File path helpers
=================

**Energy.Base.Path** is helper class for working with file paths.

Let's look at these example paths.

```
C:\Program Files\\\.\file.txt
C:\"Program Files"\
/mnt/c/"name / slash"/'file/1'.txt
/mnt/c\\sub\\..\\/" "" "/'file\'2\''.txt
```

Split
-----

Split path into parts.
Each part will contain trailing directory separator characters.

When using without specified allowed slashes it will by default allow to use both Unix-style slash characters or Windows-style backslash character as path separator.

When using without specified quotation marks it will recognize apostrophes, quotes and backticks.


```csharp
string[] Energy.Base.Path.Split(string path)
```

```csharp
string[] Energy.Base.Path.Split(string path, Energy.Base.Path.SplitFormat format)
```

```csharp
string[] Energy.Base.Path.Split(string path, string[] slashes, string[] quotes
    , bool? doublets, bool? cstyle)
```

When spliting ``/home/desktop/file.txt`` it will return array with four elements.

```
/
home/
desktop/
file.txt
```

For ``C:\Users\Desktop\file.txt`` it will work similar way.

```
C:\
Users\
Desktop\
file.txt
```

Walk
----

Walk through relative path and return absolute path without any dot folders.

Function will also strip repeated path separators.

```csharp
string Energy.Base.Path.Walk(string path, string directory)
```

If second parameter is omitted, current directory will be used for relative paths.

```csharp
string Energy.Base.Path.Walk(string path)
```

ChangeSeparator
---------------

Change any directory separator to native one for compatibility.

```csharp
string Energy.Base.Path.ChangeSeparator(string path)
```

IsSeparator
-----------

Check if file path part is directory separator or not.

Multiple separator characters are allowed.

```csharp
bool Energy.Base.Path.IsSeparator(string part, string[] slashes)
```

```csharp
bool Energy.Base.Path.IsSeparator(string part, string slashes)
```

```csharp
bool Energy.Base.Path.IsSeparator(string part)
```

StripQuotation
--------------

Strip quotation marks from file path.

Converts C:\"Program Files"\"Dir" into C:\Program Files\Dir.

```csharp
string Energy.Base.Path.StripQuotation(string path)
```

Environment
-----------

Get array of directories from environment variable using specifed separator character.

```csharp
string Energy.Base.Path.Environment(string variable, char separator)
```

Get array of directories from environment variable.

```csharp
string Energy.Base.Path.Environment(string variable)
```

Get array of directories from ``PATH`` environment variable.

```csharp
string Energy.Base.Path.Environment()
```
