Naming convention
=================

These functions may be helpful to write convention tests.

Please keep in mind that when validating, leading special characters like ``@``, ``_`` or ``$`` are not allowed as identifiers must start with letters only.

CamelCase
---------

Return words capitalized with first word in lower case.

``camelCase``

```csharp
string Energy.Base.Naming.CamelCase(string[] words)
```

Check if text is valid identifier written in camelCase.

```csharp
bool Energy.Base.Naming.IsCamelCase(string text)
```

CobolCase
---------

Return words upper case, separated with hyphen character.

``COBOL-CASE``

```csharp
string Energy.Base.Naming.CobolCase(string[] words)
```

Check if text is valid identifier written in COBOL-CASE.

```csharp
bool Energy.Base.Naming.IsCobolCase(string text)
```

ConstantCase
------------

Return words upper case, separated with underscore character.

``CONSTANT_CASE``

```csharp
string Energy.Base.Naming.ConstantCase(string[] words)
```

Check if text is valid identifier written in CONSTANT_CASE.

```csharp
bool Energy.Base.Naming.IsConstantCase(string text)
```

DashCase
--------

Return words lower case, separated with hyphen character.

``dash-case``

This style is often used in URLs to give more human-readable look.

Also known as kebab-case or hyphen-case.

```csharp
string Energy.Base.Naming.DashCase(string[] words)
```

Check if text is valid identifier written in dash-case.

```csharp
bool Energy.Base.Naming.IsDashCase(string text)
```

HyphenCase
----------

Return words lower case, separated with hyphen character.

``hyphen-case``

This style is often used in URLs to give more human-readable look.

Also known as dash-case or kebab-case.

```csharp
string Energy.Base.Naming.HyphenCase(string[] words)
```

Check if text is valid identifier written in hyphen-case.

```csharp
bool Energy.Base.Naming.IsHyphenCase(string text)
```

KebabCase
---------

Return words lower case, separated with hyphen character.

``kebab-case``

```csharp
string Energy.Base.Naming.KebabCase(string[] words)
```

Check if text is valid identifier written in kebab-case.

```csharp
bool Energy.Base.Naming.IsKebabCasee(string text)
```

PascalCase
----------

Return words capitalized.

``PascalCase``

```csharp
string Energy.Base.Naming.PascalCase(string[] words)
```

Check if text is valid identifier written in PascalCase.

```csharp
bool Energy.Base.Naming.IsPascalCase(string text)
```

SnakeCase
---------

Return words lower case, joined with underscore character.

``snake_case``

Also known as underscore_case.

```csharp
string Energy.Base.Naming.SnakeCase(string[] words)
```

Check if text is valid identifier written in camelCase.

```csharp
bool Energy.Base.Naming.IsSnakeCase(string text)
```

TrainCase
---------

Return words capitalized and separated by hyphen character.

``Train-Case``

```csharp
string Energy.Base.Naming.TrainCase(string[] words)
```

Check if text is valid identifier written in Train-Case.

```csharp
bool Energy.Base.Naming.IsTrainCase(string text)
```

UnderscoreCase
--------------

Return words lower case, joined with underscore character.

``underscore_case``

Also known as snake_case.

```csharp
string Energy.Base.Naming.UnderscoreCase(string[] words)
```

Check if text is valid identifier written in underscore_case.

```csharp
bool Energy.Base.Naming.IsUnderscoreCase(string text)
```
