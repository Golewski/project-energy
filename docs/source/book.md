Book
====

Code at first sight
-------------------

Referencing "Energy.Core" library there 

Case: Service
-------------

Case: Application
-----------------

Borrow, play, throw away
------------------------

Use it as you like.

Coding tips
-----------

### Importing namespace ###

Warning about importing namespaces with "using" keyword.
Consider always using full namespace or import using additional aliases for better future code maintenance in case of unpredictable changes like ESS.

```csharp
using Energy.Enumeration as EE;
```

```csharp
var textAlign = EE.TextAlign.Justify;
```
