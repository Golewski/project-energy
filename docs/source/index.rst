Energy Core Library
===================

.. toctree::
   :hidden:
   :maxdepth: 4
   :titlesonly:
   
   introduction
   standard/index
   base-cast
   base-text
   base-class
   base-command
   base-number
   base-hash
   base-clock
   base-file
   base-path
   base-url
   base-anonymous
   base-collection
   base-connectionString
   base-expression
   base-queue
   base-xml
   base-json
   base-bit
   base-bytearraybuilder
   base-compression
   base-hex
   base-geo
   base-naming
   enumeration
   core-tilde
   core-tilde-color
   core-web
   core-source
   core-benchmark
   core-text
   core-syntax
   core-memory
   core-program
   core-query
   core-worker
   core-application
   example-rest
   example-webapi
   book
   license

.. raw:: html

   <style type="text/css">
   img.align-center {
     margin-left: auto;
     margin-right: auto;
     display: block;
   }
   </style>

.. image:: ../media/logo.png
   :width: 400px
   :align: center

Credit for the logo goes to Damian Zaleski **zal3wa** at gmail.com.

About
=====

**Energy.Core** is a **.NET** class library which boosts your program with functionality
covering various type conversions, utility classes, database abstraction layer,
object mapping and simple application framework.

To be used by wide range of applications, services, web or console programs.

☢️ Filled with radioactive rays ☢️

Designed for all purposes as a set of different classes compiled into one library file.

It has minimal set of external dependencies and is available for several .NET platform versions.

Supports multithreading and asynchronous design patterns.

Compatibility
-------------

It's compatible with classic **.NET** plaftform versions **.NET 2.0**, **.NET 3.5**, **.NET 4.0** and later.

Supports **.NET Core** platform with minimum version **.NET Standard 2.0**.

**.NET Compact Framework** is also supported, available as separate download.

Some parts of library may use additional **.NET** libraries like *System.Xml.Serialization*. 

Download
========

The latest builds can be found at `NuGet <https://www.nuget.org/packages/energy.core>`_.

Package for Windows CE development needs to be downloaded directly from `project page <https://github.com/zoltraks/energy-core-csharp/tree/master/download>`_.
