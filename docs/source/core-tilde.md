Tilde coloring engine
=====================

Introduction
------------

Simple elegant text coloring engine for console programs.
Based on **Empty Page #0** color engine.

Color may be specified by its number or name surrounded by tilde (~) character.
One tilde followed by a character other than a letter or number or a fragment consisting of two or more tilde characters will not be formatted.

Colors that can be used are the same as on a standard command console. There are 15 different colours plus black.

Dark colour identifiers are preceded by the letter 'd' (dark).
In a similar way, bright-colour identifiers can be preceded by the letter 'l' (light).

Color changes one by one are silently ignored. Only the last defined color will be used.

Current color is remembered before writing color text and restored after writing. Special *color* ~0~ may be used to force setting back original color inside text.

Examples
--------

```csharp
    Energy.Core.Tilde.WriteLine("~yellow~Hello, ~cyan~world~white~!");
```

![](../media/tilde01.png)

```csharp
    Energy.Core.Tilde.Write(" ~1~{1}~2~{2}~3~{3}~4~{4}~5~{5}~6~{6} ", null
        , 1, 2, 3, 4, 5, 6);
```

![](../media/tilde02.png)

```csharp
    Energy.Core.Tilde.WriteLine("You can use ~`~yellow~`~ to mark text ~yellow~yellow~0~.");
```

![](../media/tilde03.png)

```csharp
    Energy.Core.Tilde.WriteLine("~yellow~Welcome to ~blue~ReadLine ~yellow~example~white~!");
    Console.ForegroundColor = ConsoleColor.Magenta;
    Energy.Core.Tilde.Write("Write ~c~something~0~: ~magenta~");
    while (true)
    {
        string input = Energy.Core.Tilde.ReadLine();
        if (input == null)
        {
            System.Threading.Thread.Sleep(500);
            continue;
        }
        else
        {
            Energy.Core.Tilde.WriteLine("You have written ~green~{0}~0~...", input);
            break;
        }
    }
    Energy.Core.Tilde.Pause();
```

![](../media/tilde04.png)

```csharp
try
{
    throw new NotSupportedException();
}
catch (Exception exception)
{
    Energy.Core.Tilde.WriteException(exception, true);
}
```

![](../media/tilde05.png)

Pause
-----

Writes out pause text and waits for user to input anything by reading line from console.

```csharp
void Energy.Core.Tilde.Pause()
```

Break
-----

Write out one break line and set default text color.

```csharp
void Energy.Core.Tilde.Break()
```

Write out empty lines and set default text color.

```csharp
void Energy.Core.Tilde.Break(int count)
```

Write out ruler line surrounded by empty lines.

```csharp
void Energy.Core.Tilde.Break(int padding, string line)
```

ReadLine
--------

Read line from console if available. Does not wait for user to enter anything so may be useful in loops. 
Thread safe.
Returns null if user did not press Enter key.

```csharp
string Energy.Core.Tilde.ReadLine()
```

```csharp
Energy.Core.Tilde.WriteLine("~yellow~Welcome to ~blue~ReadLine ~yellow~example~white~!");
Console.ForegroundColor = ConsoleColor.Magenta;
Energy.Core.Tilde.Write("Write ~c~something~0~: ~magenta~");
while (true)
{
    string input = Energy.Core.Tilde.ReadLine();
    if (input == null)
    {
        Thread.Sleep(500);
        continue;
    }
    else
    {
        Energy.Core.Tilde.WriteLine("You have written ~green~{0}~0~..."
            , input);
        break;
    }
}
```

![](../media/tilde06.png)

Input
-----

Write out prompt message and wait for input string. 
If empty string is read from console, function will return *defaultValue* parameter value.

If message contains placeholder **{0}**, it will be
replaced with *defaultValue* parameter value.

```csharp
public static string Input(string message, string defaultValue)
```

Exception
---------

Write out exception message with optional stack trace.

```csharp
public static void Exception(Exception exception, bool trace)
```

![](../media/tilde05.png)

Strip
-----

Strip tildes from text.

```csharp
public static string Strip(string text)
```

Escape
------

Escape string which may contain tilde characters.

```csharp
public static string Escape(string text)
```

Length
------

Return total length of tilde string.
Doesn't count tilde control strings.

```csharp
public static int Length(string example)
```

Color
-----

Tilde coloring engine console color string table.

```csharp
public class Color
{
    public static string DarkBlue = "~1~";
    public static string DarkGreen = "~2~";
    public static string DarkCyan = "~3~";
    public static string DarkRed = "~4~";
    public static string DarkMagenta = "~5~";
    public static string DarkYellow = "~6~";
    public static string Gray = "~7~";
    public static string DarkGray = "~8~";
    public static string Blue = "~9~";
    public static string Green = "~10~";
    public static string Cyan = "~11~";
    public static string Red = "~12~";
    public static string Magenta = "~13~";
    public static string Yellow = "~14~";
    public static string White = "~15~";
    public static string Black = "~16~";
}
```

<!-- // not supported for commonmark

Color table
-----------

List of available colors:

| Numeric | Long          | Short  |                         |
|---------|---------------|--------|-------------------------|
| ~0~     | ~default~     |        | Default color           |
| ~1~     | ~darkblue~    | ~db~   | Dark blue               |
| ~2~     | ~darkgreen~   | ~dg~   | Dark green              |
| ~3~     | ~darkcyan~    | ~dc~   | Dark cyan               |
| ~4~     | ~darkred~     | ~dr~   | Dark red                |
| ~5~     | ~darkmagenta~ | ~dm~   | Dark magenta            |
| ~6~     | ~darkyellow~  | ~dy~   | Dark yellow             |
| ~7~     | ~gray~        | ~s~    | Gray / Silver           |
| ~8~     | ~darkgray~    | ~ds~   | Dark gray / Dark silver |
| ~9~     | ~blue~        | ~b~    | Blue                    |
| ~10~    | ~green~       | ~g~    | Dark yellow             |
| ~11~    | ~cyan~        | ~c~    | Cyan                    |
| ~12~    | ~red~         | ~r~    | Red                     |
| ~13~    | ~magenta~     | ~m~    | Magenta                 |
| ~14~    | ~yellow~      | ~y~    | Yellow                  |
| ~15~    | ~white~       | ~w~    | White                   |
| ~16~    | ~black~       | ~k~    | Black / Carbon          |
-->

Hints
-----

For escaping tilde character in text use grave accent character ~\` ... \`~.

For example ~\`~color~\`~ will be printed as ~color~.
You may optionally use double grave accent character \`\` to include hash sign inside brackets ~\` \`\` \`~.

