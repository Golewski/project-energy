Example: REST
=============

This page contains REST client examples.

GET
---

Perform GET and return response from HTTP request.

```csharp
string url = "https://www.google.com/search?q=Energy";
string body = Energy.Core.Web.Get(url).Body;
```

POST
----

Perform POST and return response from HTTP request.

```csharp
string url = "http://localhost:12345/api/documents/";
string json = @"{ ""key1"": ""value1"" }";
string[] headers = new string[] { "X-Token", "1234567890" };
Energy.Base.Http.Response response;
response = Post(url, json, "application/json", headers); 
```

PUT
---

Perform PUT and return response from URL.

```csharp
Energy.Core.Web.IgnoreCertificateValidation = true;
string url = "https://reqres.in/api/users";
string body = 
@"{
    ""name"": ""morpheus"",
    ""job"": ""leader""
}";
Energy.Base.Http.Response response;
response = Energy.Core.Web.Put(url, body, "application/json");
```

Execute
-------

More generic example.

```csharp
var request = new Energy.Base.Http.Request("POST", methodUrl);
request.Encoding = System.Text.Encoding.UTF8;
request.Body = "{ \"text\": \"Gęś\" }";
request.ContentType = "application/javascript";
request.Headers.Add("X-Path: x-path");
var response = Energy.Core.Web.Execute(request);
```

Common problems
---------------

If POST or PUT doesn't work well, you might want to make sure you are specifying value for Content-Type like in the following example. While recommended way to perform web request is to use generic one (Request and Execute), you are able to set some headers using other methods.

```csharp
string url = "https://reqres.in/api/users";
string body = 
@"{
    ""name"": ""morpheus"",
    ""job"": ""leader""
}";
Energy.Base.Http.Response response;
Energy.Core.Web.IgnoreCertificateValidation = true;
response = Energy.Core.Web.Post(url, body, "application/json");
response = Energy.Core.Web.Patch(url, body, "application/json");
response = Energy.Core.Web.Put(url, body, "application/json");
```