Program utilities
=================

GetAssembly
-----------

Get current assembly from GetExecutingAssembly or GetCallingAssembly.

This function will not throw any exception, returning null on any error.

```csharp
System.Reflection.Assembly Energy.Core.Program.GetAssembly()
```

GetExecutionFile
----------------

Get execution file location from current working assembly (calling or executing).

```csharp
System.Reflection.Assembly Energy.Core.Program.GetExecutionFile()
```

GetExecutionDirectory
---------------------

Get execution directory from the assembly location.

Resulting directory will contain trailing path separator.

```csharp
System.Reflection.Assembly Energy.Core.Program.GetExecutionDirectory(System.Reflection.Assembly assembly)
```

```csharp
System.Reflection.Assembly Energy.Core.Program.GetExecutionDirectory()
```

GetCommandName
--------------

Get short command name from assembly location.

```csharp
System.Reflection.Assembly Energy.Core.Program.GetCommandName(System.Reflection.Assembly assembly)
```

```csharp
System.Reflection.Assembly Energy.Core.Program.GetCommandName()
```

SetLanguage
-----------

Set specified language for program.

```csharp
System.Globalization.CultureInfo Energy.Core.Program.SetLanguage(string culture)
```

Set default language for program (en-US).

```csharp
System.Globalization.CultureInfo Energy.Core.Program.SetLanguage()
```

SetConsoleEncoding
------------------

Set specified encoding for console.

```csharp
System.Text.Encoding Energy.Core.Program.SetConsoleEncoding(System.Text.Encoding encoding)
```

```csharp
System.Text.Encoding Energy.Core.Program.SetConsoleEncoding(string encoding)
```

Set encoding for console to UTF-8.

```csharp
System.Text.Encoding Energy.Core.Program.SetConsoleEncoding()
```
