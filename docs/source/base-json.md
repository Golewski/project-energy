JSON functions
==============

Escape
------

Escape special characters for JSON value.

```
Backspace is replaced with '\b'.
Form feed is replaced with '\f'.
Newline is replaced with '\n'.
Carriage return is replaced with '\r'.
Tab is replaced with '\t'.
Double quote is replaced with '\"'.
Backslash is replaced with '\\'.
```

Escaped text must be surrounded with double quotes.

```csharp
string Energy.Base.Json.Escape(string text)
```

Unescape
--------

Strip backslashes from previously escaped value.

```csharp
string Energy.Base.Json.Unescape(string text)
```

Quote
-----

Represents text in double quotes and escapes special characters.

Null strings are represented as single word "null".

```csharp
string Energy.Base.Json.Quote(string text)
```

Strip
-----

Strip double quotes and unescapes string.

```csharp
string Energy.Base.Json.Strip(string text)
```
