File and directory functions
============================

**Energy.Base.File** class is a collection of file and directory utility functions.

MakeDirectory
-------------

Create directory if not exists.
Returns true if a directory exists or has been created.

```csharp
bool Energy.Base.File.MakeDirectory(string path)
```

RemoveDirectory
---------------

Remove directory if exists.
Returns true if directory has been removed or not exists.

```csharp
bool Energy.Base.File.RemoveDirectory(string path, bool recursive)
```

Remove directory if exists and is empty.

```csharp
bool Energy.Base.File.RemoveDirectory(string path)
```

GetBaseDirectory
----------------

Gets the base directory that the assembly resolver uses to probe for assemblies.

```csharp
string Energy.Base.File.GetBaseDirectory()
```

GetBasePath
----------------

Gets the base directory that the assembly resolver uses to probe for assemblies.
Return path with trailing directory separator.

```csharp
string Energy.Base.File.GetBasePath()
```

Locate
------

Locate file or executable in directories from ``PATH`` environment variable.

If file can't be found, empty string will be returned.

```csharp
string Energy.Base.File.Locate(string command)
```

Locate file or executable in search directories.

```csharp
string Energy.Base.File.Locate(string command, string[] search)
```

Locate file with one of possible extensions in search directories.

```csharp
string Energy.Base.File.Locate(string file, string[] search, string[] extension)
```

Locate file with one of possible extensions in search directories.

This version of function allows to specify lookup behaviour (iterate over directories or extensions).

```csharp
string Energy.Base.File.Locate(string file, string[] search, string[] extension, Energy.Enumeration.LocateBehaviour behaviour)
```

Locate file with one of possible extensions in search directory.

This version of function allows to specify lookup behaviour (iterate over directories or extensions).

```csharp
string Energy.Base.File.Locate(string[] list, string[] search, string[] extension, Energy.Enumeration.LocateBehaviour behaviour)
```

GetHomeDirectory
----------------

Get absolute path of home directory for current user.

Resulting path will include trailing directory separator.

```csharp
string Energy.Base.File.GetHomeDirectory()
```
