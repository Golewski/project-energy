﻿Example: WebApi
===============

Basic example
-------------

Let't create new .NET project using dotnet and one of templates for web applications.
Basic one.

```
dotnet new webapi --auth None -o AspNetCoreApi
```

If you do this for first time, take a look at SDK installed for .NET.

<div>

![](../media/cmd_dotnet_info.png)

</div>

```
cd AspNetCoreApi
```

```
dotnet build
```

If you see "Build succeded", project is ready to work on.

If you want to use just HTTP, you may comment out this line of code in your **Configure** method of your **Startup.cs**:

```csharp
//app.UseHttpsRedirection();
```

### Advanced example ###

Add package **Energy.Core** to your project.

```
dotnet add package Energy.Core
```

This example uses MySQL database, so additionally we need to install connector package.

```
dotnet add package MySql.Data
```
