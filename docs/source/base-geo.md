Geographic coordinate system
============================

Point
-------

Location point.

```csharp
class Energy.Base.Geo.Point
{
    [XmlElement("Y")]
    [DefaultValue(0)]
    public double Latitude;

    [XmlElement("X")]
    [DefaultValue(0)]
    public double Longitude;

    [XmlElement("Z")]
    [DefaultValue(0)]
    public double Altitude;
}
```

GetDistance
-----------

Get earth distance between two locations in kilometers.

```csharp
double Energy.Base.Geo.GetDistance(Energy.Base.Geo.Point location1, Energy.Base.Geo.Point location2)
```

Get distance between two locations using globe radius value.

```csharp
double Energy.Base.Geo.GetDistance(Energy.Base.Geo.Point location1, Energy.Base.Geo.Point location2, double R)
```
