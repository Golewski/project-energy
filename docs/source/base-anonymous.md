Anonymous types
===============

Namespace containing short definitions of anonymous functions useful for older .NET environment.

These types are used internally by library.

String
------

Represents function that only output string.

```csharp
public delegate string String();
```

State
-----

Represents function that changes state.

```csharp
public delegate TState State<TState>(TState input);
```

Function
--------

Multiple definitions of generic function delegate.

Alternative for Func which doesn't appear in old .NET versions. 

```csharp
public delegate TOut Function<TIn, TOut>(TIn input);
```

```csharp
public delegate void Function<TIn>(TIn input);
```

```csharp
public delegate void Function();
```

First type parameter is always related to the result, next ones for input parameters.

```csharp
Energy.Base.Anonymous.Function<int, int, int> sum = (x, y) => x + y;
```

Event
-----

Event function delegates.

```csharp
public delegate void Event();
```

```csharp
public delegate void Event<TEvent>(TEvent argument);
```
