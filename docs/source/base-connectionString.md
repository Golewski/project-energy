Connection string
=================

Inspired by classics, a class representing a connection string to a data source.
Simply, **ODBC style** *connectionString*.

Accessors
---------

Gets or sets the value associated with the specified key.
Case insensitive.

```csharp
string this[string key]
```

Properties
----------

Catalog name taken from one of alternatives:
"Database", "Database Name", "Initial Catalog".

```csharp
string Catalog
```

Server name taken from one of alernatives:
"Data Source", "Server", "DataSource", "Server Name", "Dbq".

```csharp
string Server
```

Protocol.

```csharp
string Protocol
```

User name taken from one of alernatives:
"User", "User ID".

```csharp
string User
```

Utility functions
-----------------

Quote connection string value if needed.

This method will affect on values containing space, semicolon, apostrophe or quotation mark.

```csharp
string Energy.Base.ConnectionString.Quote(string value)
```

Strip quotes from connection string value.

```csharp
string Energy.Base.ConnectionString.Unquote(string value)
```

Escape connection string value if needed.

```csharp
string Energy.Base.ConnectionString.Escape(string key)
```

Represent ODBC Connection String as DSN.

```csharp
string ToDsnString()
```

More to be add. Please fill the form if you need more.
