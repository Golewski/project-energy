Memory management
=================

GetCurrentMemoryUsage
---------------------

Get current memory usage.

```csharp
long Energy.Core.Memory.GetCurrentMemoryUsage()
```

Clear
-----

Clear MemoryStream object.

```csharp
void Energy.Core.Memory.Clear(MemoryStream stream)
```
