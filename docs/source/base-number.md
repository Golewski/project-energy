Number functions
==============

Mathematical functions.

Median
------

Return the middle number from an array.

```csharp
int Energy.Base.Number.Median(int[] array)
```

```csharp
int Energy.Base.Number.Median(int[] array, bool sort)
```

```csharp
long Energy.Base.Number.Median(long[] array)
```

```csharp
long Energy.Base.Number.Median(long[] array, bool sort)
```

```csharp
ulong Energy.Base.Number.Median(ulong[] array)
```

```csharp
ulong Energy.Base.Number.Median(ulong[] array, bool sort)
```

```csharp
double Energy.Base.Number.Median(double[] array)
```

```csharp
double Energy.Base.Number.Median(double[] array, bool sort)
```

```csharp
decimal Energy.Base.Number.Median(decimal[] array)
```

```csharp
decimal Energy.Base.Number.Median(decimal[] array, bool sort)
```
