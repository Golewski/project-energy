URL support
===========

Objects of **Energy.Base.Url** class are used to represent URL (Uniform Resource Locator) network address parts.

Explode
-------

Create URL object from string.

```csharp
Energy.Base.Url Energy.Base.Url.Explode(string url)
```

You may also use implicit operator from string as in the following example.

```csharp
Energy.Base.Url url = "http://google.com";
```

Make
----

Make URL address overriding parts of it.

Pass null as parameter to skip it or empty value to remove specified part from URL.

Last parameter *value* may be used to replace placeholder {0} with specified value.

```csharp
string Energy.Base.Url.Make(string url, string scheme, string host, string port
    , string path, string query, string fragment, string user, string password
    , string value)
```

```csharp
string Energy.Base.Url.Make(string url, string scheme, string host, int port
    , string path, string query, string fragment, string user, string password
    , string value)
```

```csharp
string Energy.Base.Url.Make(string url, string scheme, string host, string port
    , string path, string query, string fragment, string value)
```

```csharp
string Energy.Base.Url.Make(string url, string scheme, string host, int port
    , string path, string query, string fragment, string value)
```

```csharp
string Energy.Base.Url.Make(string url, string scheme, string host, string port
    , string path, string query, string fragment)
```

```csharp
string Energy.Base.Url.Make(string url, string scheme, string host, int port
    , string path, string query, string fragment)
```

Combine
-------

Combine parts of URL together. Two or more parts are concatenated with slash.

```csharp
string Energy.Base.Url.Combine(params string[] parts)
```

When joining function will discover if parameter part was started after question mark.
Parameter parts are contatenated with ampersand character instead of slash.

```csharp
string url;
url = Energy.Base.Url.Combine("https://www.youtube.com", "watch?v=NHCgbs3TcYg");
Console.WriteLine(url);
url = Energy.Base.Url.Combine("https://www.youtube.com", "watch?v=NHCgbs3TcYg", "t=150");
Console.WriteLine(url);
```

SetHost
-------

Set host name or address in URL.

```csharp
string Energy.Base.Url.SetHost(string url, string host)
```

SetPort
-------

Set port number in URL.

```csharp
string Energy.Base.Url.SetPort(string url, string port)
```

Set port number in URL.
When port number is not in range 1 .. 65535 it will be considered undefined and will be removed.

```csharp
string Energy.Base.Url.SetPort(string url, int port)
```

SetHostAndPort
--------------

Set host name or address and port number in URL.

```csharp
string Energy.Base.Url.SetHostAndPort(string url, string host, string port)
```

Set host name or address and port number in URL.
When port number is not in range 1 .. 65535 it will be considered undefined and will be removed.

```csharp
string Energy.Base.Url.SetHostAndPort(string url, string host, int port)
```

Overwrite
---------

Combine two URL objects, overwriting all or only empty parts from second one.

When *all* parameter is true, values will always be overwritten with not empty parameters from second address. Otherwise, only empty values will be overwritten.

```csharp
Energy.Base.Url Energy.Base.Url.Overwrite(Url url1, Url url2, bool all)
```

By default *all* parameter is considered to be false.

```csharp
Energy.Base.Url Energy.Base.Url.Overwrite(Url url1, Url url2)
```

This method is also available as a method of object.

```csharp
Energy.Base.Url Overwrite(Energy.Base.Url url, bool overwrite)
```

```csharp
Energy.Base.Url Overwrite(Energy.Base.Url url)
```

IsEmpty
-------

Returns true if URL is empty.

```csharp
bool IsEmpty
```

ToString
--------

Represent URL object as string.

```csharp
string ToString()
```

IsUnreserved
------------

Check if character is unreserved (allowed) character in URI according to RFC 3986.

https://tools.ietf.org/html/rfc3986

```csharp
bool Energy.Base.Url.IsUnreserved(char c)
```

```csharp
bool Energy.Base.Url.IsUnreserved(byte c)
```

Encode
------

Encode special characters for URL string, according to RFC 3986.

```csharp
string Energy.Base.Url.Encode(string text)
```

```csharp
string Energy.Base.Url.Encode(string text, Encoding encoding)
```

Escape
------

Escape all but unreserved characters for URI string with percentage codes.

```csharp
string Energy.Base.Url.Escape(string text)
```

Unescape
--------

Unescape previously encoded string from percentage codes.

```csharp
string Energy.Base.Url.Unescape(string text)
```
