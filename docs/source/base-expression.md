Regular expressions
===================

GetGroupDescription
-------------------

Get group description list from regular expression match.

```
    [0]: {0. (0) = "CHARACTER VARYING " 0:18}
    [1]: {1. (type) = "CHARACTER" 0:9}
    [2]: {2. (parameter) 0:0}
    [3]: {3. (size) 0:0}
    [4]: {4. (extra) 0:0}
    [5]: {5. (null) 0:0}
    [6]: {6. (default) 0:0}
    [7]: {7. (option) = "VARYING " 10:8}
    [8]: {8. (value) 0:0}
```

```csharp
Energy.Base.Expression.Class.GroupDescription GetGroupDescription(string pattern, string value, RegexOptions option)
```

```csharp
Energy.Base.Expression.Class.GroupDescription GetGroupDescription(Regex regex, Match match)
```

```csharp
Energy.Base.Expression.Class.GroupDescription GetGroupDescription(string pattern, Match match)
```

```csharp
Energy.Base.Expression.Class.GroupDescription GetGroupDescription(Match match)
```
