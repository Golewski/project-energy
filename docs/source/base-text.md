Text functions
==============

Text related functions.

There was a small question whether the class name should be renamed from **Energy.Base.Text** to something else to avoid possible conflicts with **System.Text** when anyone wants to add **Energy.Base** namespace to *using* list.
It was decided to keep it as it is while recommending using full class names in *using* list and synonyms as well.

Constants
---------

HTML break.

```
Energy.Base.Text.BR = "<br>";
```

New line string. *CR* *LF* only for Windows, *LF* otherwise.

```
Energy.Base.Text.NL = "\r\n" | "\n"
```
  
Whitespace characters string.

```
Energy.Base.Text.WS = " \t\r\n\v";
```

An array of empty texts containing end-of-line characters.

```
Energy.Base.Text.NEWLINE_ARRAY = new string[] { "\r\n", "\n", "\r" };
```

Regular expression pattern for new line.

```
Energy.Base.Text.NEWLINE_PATTERN = "\r\n|\n|\r";
```

Quotation
---------

Introducing specialized class **Energy.Base.Text.Quotation** to help with quotation in texts.

```
"Hello ""John""..."
```

Use static method **Energy.Base.Text.Quotation.From** to create object directly from text identifier.

```csharp
var q = Energy.Base.Text.Quotation.From("<<>>");
```

This method will create quotation definition object from a string. 
If string is null or empty, null will be returned.

If string contains only 1 character, it would be treated as prefix and suffix character,
double suffix character will be used as escape sequence.
If definition contains spaces but not starts with, it will be splited by it. 
First element of such array will be used as prefix, last one as suffix,
and all elements between them will be treated as escape sequences.
If the number of characters is even, first half will be treated as prefix, second as suffix,
double suffix will be treated as escape sequence.
If the number of characters is odd, middle character will be treated as escape character for
suffix sequence of characters. It's common to use backslash there.

Example | Description
------- | -----------
"'"     | Apostrophe will be used as prefix and suffix, double apostrophes are allowed.
"$%"    | Dollar will be used as prefix and percentage as suffix, double percentages are allowed.
"[[]]"  | [[ will be used as prefix and ]] as suffix, ]]]] will be treated as ]].
"%/%"   | Percentage will be used as prefix and suffix, / will be treated as escape character, so /% sequence is allowed.

Cut
---

Cut or return part of text which ends with one of ending sequences, supporting optional quotations.

If text contains part in quotes, it will be included as is, together with quotation characters until ending sequence is found.

When cutting text "a$b$Hello '$'$d" by dollar sign ($) as ending and apostrophes (') as quotations text will be cutted in following pieces: "a", "b", "Hello '$'", "d".

Parameter | Description
--------- | -----------
**terminator** | Array of possible ending sequences.
**quotation** | Array of possible quotations, written in the form used by Energy.Base.Text.Quotation.From ("'", "''", @"'\'", @"' '' \' '", "[]", "%", ...).

```csharp
string Energy.Base.Text.Cut(string text, string[] terminator, string[] quotation)
```

When passing text as reference, original object will be replaced with what left. Use it in a loop until resulting text is empty to cut all parts.

```csharp
string Energy.Base.Text.Cut(ref string text, string[] terminator, string[] quotation)
```

Exchange
--------

Exchange texts or characters between each other.

```csharp
void Energy.Base.Text.Exchange(ref string first, ref string second)
```

```csharp
void Energy.Base.Text.Exchange(ref char first, ref char second)
```

Select
------

Select first non empty string element.

```csharp
string Energy.Base.Text.Select(params string[] list)
```
        
Cell
----

Align and limit the text to the specified size. 
Cut the initial characters from the text value. 
If there are enough space, add a prefix and a suffix in order from the alignment direction of the text.

Some of parameters used are:

* *text* : **string**
  
  Text value to be aligned in a cell. That's obvious.


* *start* : **int**
  
  The initial index of the text to be cut out. 
  If less than zero, it indicates the last characters of the text.


* *fill* : **char**
  
  Character that will be used if text is shorter than specified size.


* *pad* : **Energy.Enumeration.TextPad**
  
  Padding direction, may be left or right.
  Because padding is defined as flags, center or middle is also avaiable.


* *prefix* : **string**
  
  Optional prefix text that can be added if there is a space in resulting text to match size.


* *suffix* : **string**
  
  Optional suffix text that can be added if there is a space in resulting text to match size.

There is also a version of **Energy.Base.Text.Cell** function which can return remains of text that did not fit in the specified size of text.

```csharp
string Energy.Base.Text.Cell(string text, int start, int size, Energy.Enumeration.TextPad pad, char fill, string prefix, string suffix, out string remains)
```

```csharp
string Energy.Base.Text.Cell(string text, int start, int size, Energy.Enumeration.TextPad pad, out string remains)
```

```csharp
string Energy.Base.Text.Cell(string text, int start, int size, Energy.Enumeration.TextPad pad, char fill, out string remains)
```

```csharp
string Energy.Base.Text.Cell(string text, int start, int size, Energy.Enumeration.TextPad pad)
```

```csharp
string Energy.Base.Text.Cell(string text, int start, int size, Energy.Enumeration.TextPad pad, char fill)
```

```csharp
string Energy.Base.Text.Cell(string text, int size, Energy.Enumeration.TextPad pad, out string remains)
```

```csharp
string Energy.Base.Text.Cell(string text, int size, Energy.Enumeration.TextPad pad, char fill, out string remains)
```

```csharp
string Energy.Base.Text.Cell(string text, int size, Energy.Enumeration.TextPad pad)
```

```csharp
string Energy.Base.Text.Cell(string text, int size, Energy.Enumeration.TextPad pad, char fill)
```

The same with *Energy.Enumeration.TextAlign* instead of *Energy.Enumeration.TextPad*.

```csharp
string Energy.Base.Text.Cell(string text, int start, int size, Energy.Enumeration.TextAlign align, out string remains)
```

```csharp
string Energy.Base.Text.Cell(string text, int start, int size, Energy.Enumeration.TextAlign align)
```

```csharp
string Energy.Base.Text.Cell(string text, int start, int size, Energy.Enumeration.TextAlign align, char fill)
```

```csharp
string Energy.Base.Text.Cell(string text, int size, Energy.Enumeration.TextAlign align, out string remains)
```

### Example ###

```csharp
string text;
text = "Ana's Song";
string cell;
cell = Energy.Base.Text.Cell(text, 0, 3, Energy.Enumeration.TextPad.Left);
// cell is now "Ana"
cell = Energy.Base.Text.Cell(text, -4, 4, Energy.Enumeration.TextPad.Left);
// cell is now "Song"
```

EscapeExpression
----------------

Escape text for regular expression.

```csharp
string Energy.Base.Text.EscapeExpression(string text)
```

Random
------

Generate random text.
Resulting string will contain upper and lower latin letters and numbers only.
You may expect length from 3 to 10 characters.

```csharp
string Energy.Base.Text.Random()
```

Surround
--------

Surround text with prefix and suffix, optionally adding prefix only when needed.

* *text* : **string**
 
  Text to surround.


* *prefix* : **string**

  Prefix to add at begining.


* *suffix* : **string**

  Suffix to add at ending.


* *optional* : **bool**

  Add prefix and suffix only when needed.


```csharp
string Energy.Base.Text.Surround(string text, string prefix, string suffix, bool optional)
```

Texture
-------

Repeat string pattern to specified amount of characters length.

```csharp
string Energy.Base.Text.Texture(string pattern, int size)
```

Trim
----

Remove leading and trailing whitespace.
Includes space, tabulation (horizontal and vertical), new line and null characters.

```csharp
string Energy.Base.Text.Trim(string value)
```

IsWild
------

Check if string contains one of wild characters ("*" or "?").

```csharp
bool Energy.Base.Text.IsWild(string text)
```

IsLike
------

Check if string contains one of characters used in LIKE ("%" or "_").

```csharp
bool Energy.Base.Text.IsLike(string text)
```

ConvertNewLine
--------------

Convert new line delimiter to specified one.

```csharp
string Energy.Base.Text.ConvertNewLine(string text, string newLine)
```

Convert newline delimiter to environment default.
Value of constant **Energy.Base.Text.NL** is used.

```csharp
string Energy.Base.Text.ConvertNewLine(string text)
```

Join
----

Join strings into one list with separator.

For example Energy.Base.Text.Join(" : ", "A", "B", "", "C") will return "A : B : : C".

```csharp
string Energy.Base.Text.Join(string glue, bool empty, params string[] array)
```

Join non empty and optionally empty strings into one list with separator.

For example Energy.Base.Text.Join(" : ", false, "A", "B", "", "C") will return "A : B : C".

```csharp
string Energy.Base.Text.Join(string glue, params string[] array)
```

Join elements of string dictionary.

```csharp
string Energy.Base.Text.Join(string glue, string format, Dictionary<string, string> dictionary)
```

```csharp
Dictionary<string, string> d = new Dictionary<string, string>();
d["a"] = "B";
d["c"] = "D";
string s = Energy.Base.Text.Join(", ", "{0}-{1}", d); // "a-B, c-D" 
```

Join elements of string-object dictionary.

```csharp
string Energy.Base.Text.Join(string glue, string format, Dictionary<string, object> dictionary)
```

Quote
-----

Surround text with quotation characters (").

Escape existing quotation characters with additional quotation character ("").

```csharp
string Energy.Base.Text.Quote(string text)
```

Surround text with quotation characters (") optionally.

If optional parameter is true, text will be quoted only when text contains specified quotation character.
Escape existing quotation characters with additional quotation character ("").

```csharp
string Energy.Base.Text.Quote(string text, bool optional)
```

Surround text with specified quotation characters optionally.

If optional parameter is true, text will be quoted only when text contains specified quotation character.
Escape existing quotation characters with additional quotation character.

```csharp
string Energy.Base.Text.Quote(string text, string with, bool optional)
```

Surround text with specified quotation characters.

Escape existing quotation character with specified escape character.

```csharp
string Energy.Base.Text.Quote(string text, string with, string escape)
```

### Example ###

```csharp
// Excpect 'Hello\'John\'.'
Debug.WriteLine(Energy.Base.Text.Quote("Hello 'John'.", "'", "\\"));
```

Strip
-----

Strip text from double quotation marks.

Two sequential quotation marks inside the text will be replaced by single characters.

```csharp
string Energy.Base.Text.Strip(string text)
```

Strip text from quotation.

Two sequential quotation characters inside the text will be replaced by single characters.

```csharp
string Energy.Base.Text.Strip(string text, char quote)
```

```csharp
string Energy.Base.Text.Strip(string text, string quote)
```

Strip text from quotation.

Escape character for including quotation characters inside the text may be provided.

```csharp
string Energy.Base.Text.Strip(string text, char quote, char escape)
```

```csharp
string Energy.Base.Text.Strip(string text, string quote, string escape)
```

Strip text from quotation.

Escape character for including quotation characters inside the text may be provided.

```csharp
string Energy.Base.Text.Strip(string text, string start, string end, string escape, out bool change)
```

```csharp
string Energy.Base.Text.Strip(string text, string start, string end, string escape)
```

Strip text from quotation.

Escape character for including quotation characters inside the text may be provided.

Multiple variations may be set to allow different quotation styles to work.

```csharp
string Energy.Base.Text.Strip(string text, string[] start, string[] end, string[] escape, out bool change)
```

```csharp
string Energy.Base.Text.Strip(string text, string[] start, string[] end, string[] escape)
```

```Example```

```csharp
var qs = new string[] { "'", "`" };
var s1 = Energy.Base.Text.Strip("'It''s a dog eat dog world.'", qs, qs, qs);
var s2 = Energy.Base.Text.Strip("`It``s a dog eat dog world.`", qs, qs, qs);
```

```csharp
var qa = new string[] { "@\"", "\"" };
var qb = new string[] { "\"", "\"" };
var qe = new string[] { "\"\"", "\\" };
var s1 = Energy.Base.Text.Strip("@\"Verbatim \"\"style\"\" example", qa, qb, qe);
var s2 = Energy.Base.Text.Strip("\"Normal \\\"style\\\" example", qa, qb, qe);
```

HasDigitsOnly
-------------

Checks if string contains only digits.

```csharp
bool Energy.Base.Text.HasDigitsOnly(string value)
```

IsInteger
---------

Checks if string is an integer number.

```csharp
bool Energy.Base.Text.IsInteger(string value, bool negative)
```

```csharp
bool Energy.Base.Text.IsInteger(string value)
```

IsLong
------

Checks if string is a long integer number.

```csharp
bool Energy.Base.Text.IsLong(string value, bool negative)
```

```csharp
bool Energy.Base.Text.IsLong(string value)
```

EmptyIfNull
------

Return empty string when string parameter is null or string parameter itself otherwise.
This function ensures string objects are always defined.

```csharp
string Energy.Base.Text.EmptyIfNull(string value)
```

IfEmpty
-------

Returns first non empty string from a parameter list. 
Strings are considered to be empty if they are null.
Function will return empty string ("") if parameter list is empty. 

```csharp
string Energy.Base.Text.TextIfEmpty(params string[] input)
```

IfWhite
-------

Returns first non white string from a parameter list. 
White string is considered to be null, zero length or string containing only whitespace characters.
Function will return empty string ("") if parameter list is empty.

```csharp
string Energy.Base.Text.TextIfWhite(params string[] input)
```

RemoveWhite
-----------

Remove whitespace characters from entire string.

```csharp
string Energy.Base.Text.RemoveWhite(string value)
```

ContainsWhite
-------------

Check if text or character array contains whitespace.

```csharp
string Energy.Base.Text.ContainsWhite(char[] array)
```

```csharp
string Energy.Base.Text.ContainsWhite(string text)
```

ReplaceWhite
------------

Replace whitespace characters with replacement string in entire string.

```csharp
string Energy.Base.Text.ReplaceWhite(string text, string replacement)
```

Encoding
--------

Find System.Text.Encoding for specified name.

Get System.Text.Encoding.UTF8 by default or if encoding not exists.

Treats UCS-2 the same as UTF-16 besides differences.

Accepts values like "UTF-8", "UTF", "UTF8", "UNICODE", "UCS-2 LE", "UCS-2 BE", "1250", "1252", and so on.

```csharp
System.Text.Encoding Energy.Base.Text.Encoding(string encoding)
```

Capitalize
----------

Return a word with its first letter upper case and remaining letters in lower case.

```csharp
string Energy.Base.Text.Capitalize(string word)
```

Return array of words with their first letters upper case and remaining letters in lower case.

```csharp
string Energy.Base.Text.Capitalize(string word)
```

Upper
-----

Change letters in a word to upper case.

```csharp
string Energy.Base.Text.Upper(string word)
```

Change letters in word list to upper case.

```csharp
string Energy.Base.Text.Upper(string[] words)
```

The reason why this function is included is that it works with unicode.

Lower
-----

Change letters in a word to lower case.

```csharp
string Energy.Base.Text.Lower(string[] words)
```

Change letters in word list to lower case.

```csharp
string Energy.Base.Text.Lower(string[] words)
```

The reason why this function is included is that it works with unicode also.

IncludeLeading
--------------

Include leading text if not already present at the begining.

```csharp
string Energy.Base.Text.IncludeLeading(string text, string missing)
```

```csharp
string Energy.Base.Text.IncludeLeading(string text, string missing, bool ignoreCase)
```

Include leading character if not already present at the begining.

```csharp
string Energy.Base.Text.IncludeLeading(string text, char missing)
```

```csharp
string Energy.Base.Text.IncludeLeading(string text, char missing, bool ignoreCase)
```

IncludeTrailing
---------------

Include trailing text if not already present at the end.

```csharp
string Energy.Base.Text.IncludeTrailing(string text, string missing)
```

```csharp
string Energy.Base.Text.IncludeTrailing(string text, string missing, bool ignoreCase)
```

Include leading character if not already present at the end.

```csharp
string Energy.Base.Text.IncludeTrailing(string text, char missing)
```

```csharp
string Energy.Base.Text.IncludeTrailing(string text, char missing, bool ignoreCase)
```

RemoveEmptyLines
----------------

Remove empty lines from string.

```csharp
string Energy.Base.Text.RemoveEmptyLines(string text)
```

RemoveEmptyElements
-------------------

Remove empty elements from array of strings.

New array will be returned.

```csharp
string[] Energy.Base.Text.RemoveEmptyElements(string[] array)
```

Remove empty elements from list of strings.

List will be modified and returned back.

```csharp
void Energy.Base.Text.RemoveEmptyElements(List<string> list)
```

Contains
--------

Check if text contains searched string.

```csharp
bool Energy.Base.Text.Contains(string text, string search, bool ignoreCase)
```

```csharp
bool Energy.Base.Text.Contains(string text, string search)
```

Check if text representation of object contains searched string.

```csharp
bool Energy.Base.Text.Contains(object o, string search, bool ignoreCase)
```

```csharp
bool Energy.Base.Text.Contains(object o, string search)
```
