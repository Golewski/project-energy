Type conversion
===============

As
--

Generic conversion from one type to another.

```csharp
T Energy.Base.Cast.As<T>(object value)
```

```csharp
object Energy.Base.Cast.As(System.Type type, object value)
```

ObjectToDouble
--------------

Convert object to double value without exception.
Treat comma "," the same as dot "." as decimal point when converting from string.

```csharp
double Energy.Base.Cast.ObjectToDouble(object value)
```

ObjectToDecimal
---------------

Convert object to decimal number.
Treat comma "," the same as dot "." as decimal point when converting from string.

```csharp
double Energy.Base.Cast.ObjectToDouble(object value)
```

ObjectToLong
------------

Convert object to long integer number.

```csharp
long Energy.Base.Cast.ObjectToLong(object value)
```

ObjectToUnsignedLong
--------------------

Convert object to long integer number.

```csharp
ulong Energy.Base.Cast.ObjectToUnsignedLong(object value)
```

ObjectToBool
------------

Convert object to boolean value.

```csharp
bool Energy.Base.Cast.ObjectToBool(object value)
```

ObjectToJsonValue
-----------------

Convert object to formal JSON value string.

```csharp
string Energy.Base.Cast.ObjectToJsonValue(object value)
```

JsonValueToObject
-----------------

Convert JSON value string to an object.

```csharp
object Energy.Base.Cast.JsonValueToObject(string text)
```

StringToDecimal
---------------

Convert string to decimal value without exception.
Treat comma "," the same as dot "." as decimal point.

```csharp
decimal Energy.Base.Cast.StringToDecimal(string value, NumberStyles numberStyles)
```

```csharp
decimal Energy.Base.Cast.StringToDecimal(string value)
```

StringToLongSmart
-----------------

Convert string to long integer value without exception  removing numerical differences.

```csharp
long Energy.Base.Cast.StringToLongSmart(string value)
```

StringToUnsignedLongSmart
-----------------

Convert string to unsigned long integer value without exception  removing numerical differences.

```csharp
long Energy.Base.Cast.StringToUnsignedLongSmart(string value)
```

BoolToString
------------

Convert boolean value to its string representation.

```csharp
string Energy.Base.Cast.BoolToString(bool value, Energy.Enumeration.BooleanStyle style)
```

Enum
----

Convert string to enumeration value.

```csharp
object Energy.Base.Cast.StringToEnum(string value, Type type)
```

RemoveNumericalDifferences
--------------------------

Remove numerical differences from text representation of number.

Treat comma "," the same as dot "." as decimal point.

Ignore space, underscore and apostrophes between digits.

```csharp
string Energy.Base.Cast.RemoveNumericalDifferences(string value)
```

MemorySizeToString
------------------

Represents memory size as a string containing a numeric value with an attached size unit.

Units used are: "B", "KB", "MB", "GB", "TB", "PB", "EB".

```csharp
string Energy.Base.Cast.MemorySizeToString(long sizeInBytes, int decimalPlaces, bool numberCeiling)
```

IntegerToHex
------------

Convert integer to hexadecimal value.

Resulting string will have count specified by size of digits or letters (A-F).

For **int** which is the synonym of **System.Int32** resulting string will be 8 characters long.

If number representation will be larger than size, it will be truncated to the last characters.

Example: IntegerToHex(100000, 4) will result with "86a0" instead of "186a0" or "186a".

```csharp
string Energy.Base.Cast.IntegerToHex(int value)
```

HexToInteger
------------

Convert hexadecimal string to integer value (System.Int32).

```csharp
int Energy.Base.Cast.HexToInteger(string hex)
```

OctToInteger
------------

Convert octal string to integer value (System.Int32).

```csharp
int Energy.Base.Cast.OctToInteger(string oct)
```

StringToStream
--------------

Convert string to a stream using specified encoding.

```csharp
Stream Energy.Base.Cast.StringToStream(string value, Encoding encoding)
```

If encoding is not specified, UTF-8 will be used.

```csharp
Stream Energy.Base.Cast.StringToStream(string value)
```

Always try to use streams in using section to properly free allocated objects like in the following example of unit test using this function.

```csharp
string needle = "€";
byte[] buffer;

using (Stream stream = Energy.Base.Cast.StringToStream(needle))
{
    int length = (int)stream.Length;
    buffer = new byte[length];
    stream.Read(buffer, 0, length);
}
Assert.AreEqual(0, Energy.Base.Bit.Compare(new byte[] { 226, 130, 172 }, buffer));

using (Stream stream = Energy.Base.Cast.StringToStream(needle, encoding: Encoding.Unicode))
{
    int length = (int)stream.Length;
    buffer = new byte[length];
    stream.Read(buffer, 0, length);
}
Assert.AreEqual(0, Energy.Base.Bit.Compare(new byte[] { 172, 32 }, buffer));
```

StringToShort
-------------

Convert string to short integer value without exception.

```csharp
ushort Energy.Base.Cast.StringToShort(string value)
```

You may want to set additional options like allow decimal point in numbers or exceeding value.
When value exceeds maximum, reminder will be returned.

```csharp
ushort Energy.Base.Cast.StringToShort(string value, bool point, bool exceed)
```

StringToUnsignedShort
---------------------

Convert string to short integer value without exception.

```csharp
ushort Energy.Base.Cast.StringToUnsignedShort(string value)
```

You may want to set additional options like allow decimal point in numbers or exceeding value.
When value exceeds maximum, reminder will be returned. 
In addition when *exceed* parameter is true, negative values will be returned as positive.

```csharp
ushort Energy.Base.Cast.StringToUnsignedShort(string value, bool point, bool exceed)
```
