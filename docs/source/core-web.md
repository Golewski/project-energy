Web functions
=============

Certificate validation
----------------------

In some cases certificate validation has to be switched off when using self signed certificates is SSL connectivity.

You may use *Energy.Core.Web.IgnoreCertificateValidation* property to change this setting programatically.

```csharp
public static bool IgnoreCertificateValidation
```

REST
----

Formal methods are defined "HEAD", "GET", "PUT", "POST", "PATCH", "DELETE", "OPTIONS".

All REST methods use *Energy.Base.Http.Request* and *Energy.Base.Http.Response* classes which represents web request and response.

### GET ###

Perform GET and return response from URL.

```csharp
public static Energy.Base.Http.Response Get(string url)
```

```csharp
public static Energy.Base.Http.Response Get(string url, string acceptType)
```

```csharp
public static Energy.Base.Http.Response Get(Energy.Base.Http.Request request)
```

### POST ###

Perform POST and return response from HTTP request.

```csharp
public static Energy.Base.Http.Response Post(Energy.Base.Http.Request request)
```

Perform POST and return response from URL.

```csharp
public static Energy.Base.Http.Response Post(string url, object body, string contentType, string[] headers, string acceptType)
```

A little too many parameters, right?

```csharp
public static Energy.Base.Http.Response Post(string url, object body, string contentType, string[] headers)
```

```csharp
public static Energy.Base.Http.Response Post(string url, object body, string contentType)
```

```csharp
public static Energy.Base.Http.Response Post(string url, object body)
```

```csharp
public static Energy.Base.Http.Response Post(string url)
```

### PUT ###

Perform PUT and return response from URL.

```csharp
public static Energy.Base.Http.Response Put(string url, object body, string contentType, string[] headers)
```

```csharp
public static Energy.Base.Http.Response Put(string url)
```

```csharp
public static Energy.Base.Http.Response Put(string url, object body, string contentType)
```

```csharp
public static Energy.Base.Http.Response Put(string url, object body)
```

### PATCH ###

Perform PATCH and return response from URL.

```csharp
public static Energy.Base.Http.Response Patch(Energy.Base.Http.Request request)
```

```csharp
public static Energy.Base.Http.Response Patch(string url, object body, string contentType, string[] headers, string acceptType)
```

```csharp
public static Energy.Base.Http.Response Patch(string url, object body, string contentType)
```

```csharp
public static Energy.Base.Http.Response Patch(string url, object body)
```

```csharp
public static Energy.Base.Http.Response Patch(string url)
```

### HEAD ###

Perform HEAD and return response from URL.

```csharp
public static Energy.Base.Http.Response Head(Energy.Base.Http.Request request)
```

```csharp
public static Energy.Base.Http.Response Head(string url)
```

```csharp
public static Energy.Base.Http.Response Head(string url, out string[] responseHeaders)
```

### DELETE ###

Perform DELETE and return response from URL.

```csharp
public static Energy.Base.Http.Response Delete(Energy.Base.Http.Request request)
```

```csharp
public static Energy.Base.Http.Response Delete(string url)
```

### OPTIONS ###

Perform OPTIONS and return response from URL.

```csharp
public static Energy.Base.Http.Response Options(Energy.Base.Http.Request request)
```

```csharp
public static Energy.Base.Http.Response Options(string url)
```
