Application
===========

GetCommandName
--------------

Get name for command taken from shortened executable name.

### Prototype ###

```csharp
static int Energy.Core.Application.GetCommandName()
```
